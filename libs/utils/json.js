function json(data) {
  return JSON.stringify(data, null, 2);
}

module.exports = {
  json
}
