function hidden(obj, key, value) {
  Object.defineProperty(obj, key, {
    value,
    enumerable: false
  });
}

function getter(obj, key, fn) {
  Object.defineProperty(obj, key, {
    get: fn,
    enumerable: false
  });
}


function isSubClassOf(child, parent) {
  if (!child || !parent) {
    return false;
  }

  let childProto = child.prototype;
  let parentProto = parent.prototype;

  while (childProto !== Object.prototype) {
    if (childProto === parentProto) {
      return true;
    }
    childProto = childProto.__proto__;
  }
  return false;
}

module.exports = {
  hidden,
  getter,
  isSubClassOf
};
