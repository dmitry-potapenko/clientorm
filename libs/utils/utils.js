function series(funcs) {
  return funcs.reduce((prev, current) => prev.then(() => current()), Promise.resolve());
}

module.exports = {
  series
}
