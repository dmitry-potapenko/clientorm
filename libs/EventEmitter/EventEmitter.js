const { series } = require("../utils/utils");
/**
 * Classic EventEmitter
 */
class EventEmitter {
  constructor() {
    this.handlers = {};
  }

  on(event, handler) {
    if (Array.isArray(this.handlers[event]) && !~this.handlers[event].findIndex((h) => h === handler)) {
      this.handlers[event].push(handler);
    } else {
      this.handlers[event] = [handler];
    }
  }

  off(event, handler) {
    if (Array.isArray(this.handlers[event])) {
      const index = this.handlers[event].findIndex((h) => h === handler);

      if (~index) {
        this.handlers[event].splice(index, 1);
      }
    }
  }

  exec(event, payload) {
    if (Array.isArray(this.handlers[event])) {
      return series(this.handlers[event].map(h => () => h(payload)));
    }
  }
}

module.exports = EventEmitter;
