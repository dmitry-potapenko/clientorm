const Node = require("../Schema/Node");
const { hidden, getter } = require("../utils/object");

function toCapitalCase(str) {
  if (typeof str === "string") {
    return str[0].toUpperCase() + str.slice(1);
  }
  return str;
}

function applyValidator(obj, validationProps) {
  hidden(obj, "$validator", {
    ...validationProps,
    errors: []
  });
  getter(obj, "invalid", function () {
    return this.$validator.errors.length > 0;
  });
  getter(obj, "validationErrors", function () {
    return this.$validator.errors;
  });
  obj.validate = function () {
    if (this.$validator && this.$validator.validators) {
      const res = this.$validator.validators.map(fn => fn.call(this, this.value)).filter(e => e);

      if (res.length > 0) {
        this.$validator.errors = [...res];

        return {
          valid: false,
          errors: [...res]
        }
      }
    }
    return {
      valid: true,
      errors: []
    }
  }
}

class Field extends Node {
  attr(name, value) {
    if (!name) {
      throw new Error("Attribute name is not specified");
    } else if (value !== undefined) {
      return this.setAttr(name, value);
    } else {
      return this.getAttr(name, value);
    }
  }

  getAttr(name) {
    if (name in this) {
      return this[name];
    }
  }

  async setAttr(name, value) {
    const oldValue = this[name];

    if (typeof this[`set${toCapitalCase(name)}`] === "function") {
      this[name] = this[`set${toCapitalCase(name)}`](value);
    } else {
      this[name] = value;
    }

    if (typeof this.validate === "function") {
      this.validate();
    }
    return this.callHooks("change", {
      attr: name,
      prev: oldValue,
      current: value
    });
  }

  setValidator(props) {
    applyValidator(this, props);
  }

  /**
 * инициализация поля
 * @param props
 * @returns {Promise<Field>}
 */
  async init(props) {
    if (props) {
      for (const [k, v] of Object.entries(props)) {
        this[k] = v;
      }
    }

    try {
      await this.callHooks("init", props);
    } catch (e) {
      console.error(e);
    }
    return this;
  }

  serialize() {
    return this.value;
  }
}

module.exports = Field;
