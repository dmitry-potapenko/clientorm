const BaseField = require("./Field");

async function make(node, path = "", value = null) {
  const Ctor = node.type || BaseField;
  const field = new Ctor();
  field.attr("path", path);
  field.attr("value", value);
  field.setHooks(node.hooks);

  if (node.validation) {
    field.setValidator(node.validation);
  }
  return field.init(node.properties);
}

module.exports = make;
