/** === LOCAL === */
const FieldBuilder = require("../Fields/FieldBuilder");
const Field = require("../Fields/Field");
const List = require("./List");
const Item = require("./Item");
const Tree = require("./Tree");
const { isSubClassOf } = require("../utils/object");

async function builder(schema, data) {
  if (typeof schema !== "object") {
    throw new Error("schema should be an object");
  }
  return new Tree(await parser(schema, data));
}

async function parser(schemaNode, realNode, path = "", parent = null) {
  if (isSubClassOf(schemaNode.type, Field)) {
    return FieldBuilder(schemaNode, path, realNode);
  } else if (isSubClassOf(schemaNode.type, Item)) {
    return new Item().init(parseObjectNodes(schemaNode.properties, realNode, path, schemaNode), path);
  } else if (isSubClassOf(schemaNode.type, List) && schemaNode.items) {
    return makeList(schemaNode.items[0], realNode, path, schemaNode);
  }
  return parseObjectNodes(schemaNode.properties, realNode, path, schemaNode);
}

function parseObjectNodes(schemaObjProperties, realObj, path = "", parent = null) {
  const promises = [];
  const res = {};
  const realObjDuplicate = realObj || {};

  if (!schemaObjProperties) {
    return res;
  }

  for (const [key, elem] of Object.entries(schemaObjProperties)) {
    if (elem && typeof elem === "object") {
      if (realObjDuplicate instanceof Object) {
        const p = path ? `${path}.${key}` : `${key}`;
        promises.push(
          parser(elem, realObjDuplicate[key], p, parent)
            .then(data => {
              if (data) {
                res[key] = data;
              }
            })
        );
      }
    }
  }
  return Promise.all(promises).then(() => res);
}

function makeList(itemTemplate, realNode, path = "", parent = null) {
  const realNodeDuplicate = realNode || [];
  let children = [];

  for (const [key, elem] of Object.entries(realNodeDuplicate)) {
    const p = path ? `${path}.${key}` : `${key}`;
    children.push(parseObjectNodes(itemTemplate.properties, elem, p, itemTemplate));
  }
  return new List().init(Promise.all(children), path, itemTemplate);
}


builder.parseObjectNodes = parseObjectNodes;
builder.parser = parser;

module.exports = builder;
