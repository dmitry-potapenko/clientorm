const EventEmitter = require("../EventEmitter/EventEmitter");
const { hidden } = require("../utils/object");

class Node {
  setHooks(hooks) {
    hidden(this, "$hooks", new EventEmitter());

    if (this.onInit) {
      this.on("init", this.onInit);
    }

    if (this.onChange) {
      this.on("change", this.onChange);
    }

    if (typeof hooks === "object") {
      for (const [key, fn] of Object.entries(hooks)) {
        this.on(key, fn);
      }
    }
    return this;
  }

  on(event, fn) {
    this.$hooks.on(event, fn.bind(this));
  }

  off(event, fn) {
    this.$hooks.on(event, fn.bind(this));
  }

  /**
   * вызывает все хуки для события event
   * @param event
   * @param payload
   * @returns {*|RegExpExecArray}
   */
  callHooks(event, payload) {
    if (this.$hooks) {
      return this.$hooks.exec(event, payload);
    }
  }

  serialize() {
    const res = {};

    for (const [key, val] of Object.entries(this)) {
      if (val && typeof val.serialize === "function") {
        res[key] = val.serialize();
      }
    }
    return res;
  }
}

module.exports = Node;
