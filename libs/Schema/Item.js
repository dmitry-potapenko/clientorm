const Node = require("./Node");
const { hidden } = require("../utils/object");

class Item extends Node {
    async init(children, path = "") {
        hidden(this, "path", path);
        return Promise.resolve(children)
            .then(localChildren => {
                for (const [key, data] of Object.entries(localChildren)) {
                    if (data) {
                        this[key] = data;
                    }
                }
                return this;
            });
    }
}

module.exports = Item;
