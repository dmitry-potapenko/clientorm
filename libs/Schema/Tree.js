/** === GLOBAL === */
const jp = require("jsonpath");
/** === LOCAL === */
const Node = require("./Node");


class Tree extends Node {
  constructor(dataTree) {
    super();
    for (const [key, node] of Object.entries(dataTree)) {
      if (node) {
        this[key] = node;
      }
    }
  }
  /**
   * Retrieve a node from the schema tree
   * @param {String} path full path to a Node in the Schema
   */
  getNode(path) {
    return jp.value(this, path);
  }

  getParent(path) {
    return jp.parent(this, path);
  }
}

module.exports = Tree;
