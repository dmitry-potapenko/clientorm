const Node = require("./Node");
const { hidden } = require("../utils/object");

async function createItemFn(list) {
    // require is here to solve cyclic-deps issue
    const TreeBuilder = require("./TreeBuilder");
    return await TreeBuilder.parser(list.itemTemplate, null, `${list.path}.${list.lastIndex + 1}`, list);
}

class List extends Node {
    async init(children, path = "", itemTemplate = {}) {
        hidden(this, "path", path);
        hidden(this, "itemTemplate", itemTemplate);
        return Promise.resolve(children)
            .then(localChildren => {
                if (typeof localChildren === "object") {
                    for (const [key, data] of Object.entries(localChildren)) {
                        if (data) {
                            this[key] = data;
                        }
                    }
                }
                return this;
            });
    }
    push(item) {
        const newIndex = this.lastIndex > -1 ? this.lastIndex + 1 : 0;
        this[newIndex] = item;
        // TODO: call hooks
    }
    createItem(parserFn = createItemFn) {
        return parserFn(this);
    }
    get lastIndex() {
        const keysList = Object.keys(this)
            .map(key => + key)
            .filter(key => !Number.isNaN(key));
        return keysList.length > 0 ? Math.max(...keysList) : -1;
    }
    get length() {
        return Object.keys(this)
            .map(key => + key)
            .filter(key => !Number.isNaN(key))
            .length;
    }
    *[Symbol.iterator]() {
        for (const key in this) {
            if (!Number.isNaN(+key)) {
                yield this[key];
            }
        }
    }
}

module.exports = List;
