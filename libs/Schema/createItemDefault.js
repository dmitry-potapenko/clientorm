const TreeBuilder = require("./TreeBuilder");

async function createItemFn(list) {
  return await TreeBuilder.parser(list.itemTemplate, null, `${list.path}.${list.lastIndex + 1}`, list);
}

module.exports = createItemFn;
