/** === GLOBAL === */
/** === LOCAL === */
const schema = require("./TestSchema");
const data = require("./MockData");
const { TreeBuilder, Tree } = require("../../libs");
const { json } = require("../../libs/utils/json");

describe("Test builder ", () => {
  let tree = null;

  beforeAll(async () => {
    tree = new Tree(await TreeBuilder(schema, data));
  });

  test("Builder function is correct", async () => {
    expect(tree && tree.isn).toBeDefined();
  });

  test("List push", () => {
    const before = tree.accounts.account.length;
    tree.accounts.account.push({
      testObj: 123
    });
    const after = tree.accounts.account.length;
    expect(after).toBe(before + 1);
  });

  test("List create and push an item", async () => {
    const item = await tree.accounts.account.createItem();
    const nextIndex = tree.accounts.account.lastIndex + 1;
    const length = tree.accounts.account.length;
    expect(item.path).toBe("accounts.account." + nextIndex);
    tree.accounts.account.push(item);
    expect(tree.accounts.account.length).toBe(length + 1);
  });

  test("Tree getNode", () => {
    const node = tree.getNode(tree.accounts.account[3].path);
    expect(node.typeIsn.catalogId).toBe("ucContactType");
  });

  afterAll(() => {
    console.log("@@@parsed", json(tree));
  })
});
