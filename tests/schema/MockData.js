module.exports = {
  isn: "131232131",
  inn: "1233123213",
  accounts: {
    account: [{
      typeIsn: "11111"
    }, {
      typeIsn: "22222"
    }]
  },
  addresses: {
    address: [{
      isn1: 1
    }]
  }
}
