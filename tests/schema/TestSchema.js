const { Field, List, Item, Tree } = require("../../libs");

const schema = {
  type: Tree,
  properties: {
    isn: {
      type: Field,
      hooks: {
        init() {
          // console.log('&&&&& init ISN');
          return new Promise(r => {
            setTimeout(() => {
              // console.warn("!! isn field");
              r();
            }, 0)
          })
        }
      }
    },
    inn: {
      type: Field,
      hooks: {
        init() {
          // console.log('&&&&& init INN');
          return new Promise(r => {
            setTimeout(() => {
              // console.warn("!! isn field");
              r();
            }, 0)
          })
        }
      }
    },
    ogrn: {
      type: Field,
      hooks: {
        init() {
          // console.log('&&&&& init OGRN');
          return new Promise(r => {
            setTimeout(() => {
              // console.warn("!! OGRN field");
              r();
            }, 0)
          })
        },
        change() {
          // console.log("#@#@@ on change", this);
        }
      }
    },
    accounts: {
      type: Item,
      // перечисление вложенных структур и вспомогательныйъ свойств
      properties: {
        account: {
          type: List,
          items: [{
            type: Item,
            properties: {
              typeIsn: {
                type: Field,
                properties: {
                  catalogId: "ucContactType"
                },
                hooks: {
                  init() {
                    return new Promise(resolve => {
                      // console.log("### should load catalog 10 sec", this.catalogId);
                      setTimeout(resolve, 0);
                    });
                  }
                }
              }
            }
          }]
        }
      }
    },
    addresses: {
      type: Item,
      properties: {
        address: {
          type: List,
          items: [{
            type: Item,
            properties: {
              isn: {
                type: Field
              }
            }
          }]
        }
      }
    }
  }
}

module.exports = schema;
