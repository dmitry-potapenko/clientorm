module.exports = {
  testMatch: ["**/tests/**/*.spec.js"],
  setupFilesAfterEnv: ["./tests/jest.setup.js"]
};
