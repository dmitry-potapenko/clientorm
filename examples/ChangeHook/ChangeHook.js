const data = require("./Data");
const schema = require("./Schema");
const { TreeBuilder } = require("../../libs");

async function main() {
  const tree = await TreeBuilder(schema, data);
  const isnField = tree.getNode("isn");
  const nameField = tree.getNode("name");
  const dateField = tree.date;
  console.time("wait field change");
  await Promise.all([
    isnField.attr("value", null),
    nameField.attr("value", "Vasya"),
    dateField.attr("value", new Date())
  ]);
  console.log('#@#@#@', tree.isn.invalid, tree.isn.validationErrors);
  console.timeEnd("wait field change");
  console.log("res", tree.serialize());
}

if (require.main === module) {
  main().then(data => console.log("Finish change hook example"));
}
