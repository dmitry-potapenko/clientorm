
const moment = require("moment");
const { Field, List, Item, Tree } = require("../../libs");



class FieldChange extends Field {
  onChange(...args) {
    return new Promise(resolve => {
      setTimeout(() => {
        console.log("Lakad Matatag!", this, ...args);
        resolve();
      }, 1e3);
    })
  }
}

class FieldDate extends Field {
  setValue(value) {
    return moment(value);
  }
  serialize() {
    return moment(this.value).format("DD.MM.YY");
  }
}

const schema = {
  type: Tree,
  properties: {
    isn: {
      type: FieldChange,
      validation: {
        validators: [function (value) {
          if (value) {
            return null;
          }
          return `this field is required: ${this.path}`
        }]
      }
    },
    name: {
      type: FieldChange,
      hooks: {
        change() {
          return new Promise(resolve => {
            setTimeout(() => {
              resolve();
            }, 1500);
          })
        }
      }
    },
    date: {
      type: FieldDate
    }
  }
}

module.exports = schema;
